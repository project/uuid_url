<?php

namespace Drupal\uuid_url\Controller;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Cache\CacheableRedirectResponse;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Exception\UndefinedLinkTemplateException;
use Drupal\Tests\system\Functional\System\PageNotFoundTest;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Returns responses for uuid_url module routes.
 */
class UuidUrlController extends ControllerBase {

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Constructs a \Drupal\aggregator\Controller\AggregatorController object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Redirects to the canonical entity URL.
   *
   * @param string $entity_type
   *   The entity type ID.
   * @param string $uuid
   *   The UUID of an entity.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   The redirect.
   */
  public function uuidEntityRedirect($entity_type, $uuid) {
    try {
      $entity = $this->entityTypeManager->getStorage($entity_type)
        ->loadByProperties(['uuid' => $uuid]);
      $entity = reset($entity);
      if ($entity && $entity->access('view')) {
        // We cannot just get the URL string - we need to avoid early rendering.
        // See https://www.lullabot.com/articles/early-rendering-a-lesson-in-debugging-drupal-8
        // for details.
        $url = $entity->toUrl()->toString(TRUE);
        $response = new CacheableRedirectResponse($url->getGeneratedUrl());
        $response->addCacheableDependency($url);
        $response->addCacheableDependency($entity);
        return $response;
      }
      else {
        throw new NotFoundHttpException();
      }
    }
    catch (UndefinedLinkTemplateException $exception) {
      throw new NotFoundHttpException();
    }
    catch (PluginException $exception) {
      throw new NotFoundHttpException();
    }
  }

}
