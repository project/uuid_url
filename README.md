# UUID url

 UUID url provides a route in the form of /by_uuid/{entity_type}/{uuid} that redirects
 to standard entity url (alias or entity_type/id).